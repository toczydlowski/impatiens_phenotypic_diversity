#idea: calculate dispersion from centroid to each individual in 2014
#is degree of spread explained by genetic diversity aka do pops with more morphological diversity in a plot also have more genetic div?

#load libraries
library(dplyr)
library(tidyr)
library(ggplot2)
library(car) #for Anova 
library(purrr) #select different number of random samples from each pop
library(vegan) #for distance and dispersion calcs



#***************************************************************************************************************
#***************************************************************************************************************
#NEW SECTION - CALCULATE DISPERSION for moms - 2014 ------------------------------------------------------------

rm(list = ls())
gc()

#NOTE - we are not pairing moms and offspring here, so use all mother plants with morph data

#read in data ------
#get 2014 morph data
df.14 <- read.csv("input_data/field_2014_morphologicaldata-3-22-16.csv", header = T) %>% rename("pop" = "FIELDSITE", "samp" = "MomID.2014") %>% 
  mutate(pop_ordered = factor(pop, levels=c("4079","4082","4081","4084","4049","4050","4057","4033","4029","ALB","FW","KR"))) %>% 
  mutate(id = paste(pop_ordered,samp,sep="_")) %>% 
  dplyr::select(id,WTSHED,RIVER,pop_ordered,ht,stdia) %>% 
  mutate(year = "2014")

#read in leaf shape/size data from 2014 field sites AND 2015 arb
l <- read.csv("output_data_files/field2014ANDarb2015-10pcs_20harmonics_leafmorphdata.csv", header = T) %>% 
  mutate(pop_ordered = factor(FIELDSITE, levels=c("4079","4082","4081","4084","4049","4050","4057","4033","4029","ALB","FW","KR"))) %>% 
  separate(.,sampid,into = c("samp","x"), sep = "-", remove = F) %>% 
  mutate(sample = ifelse((is.na(x)==T),samp,x)) %>%
  dplyr::select(-x,samp,sampid) %>% 
  mutate(id = paste(pop_ordered,sample,sep="_")) %>% 
  rename(LtoW = ratio_LoverW_cm, area = area_cm2) %>% 
  mutate(SLA = area/Leaf.mass.g) %>% 
  dplyr::select(id,year,LtoW,area,SLA,PC2,PC3,PC4) %>% 
  filter(year == 2014)

head(df.14)
head(l)

d <- merge(df.14, l, all.x = T, all.y = T, by = c("id","year"))

d <- d %>% na.omit() %>% mutate(group = paste(pop_ordered,year,sep="-")) %>% filter(year == "2014")


#calculate trait dispersion ------
#put all data on a scale from 0-1, so traits with larger values don't have more influence
d.scaled <- decostand(d[,c("ht", "stdia","LtoW","area","SLA","PC2","PC3")], method = "range")
d <- cbind(d[,c("id", "year","WTSHED","RIVER","pop_ordered","group")],d.scaled)
#calculate pairwise trait distance btw each pair of plants
dist = vegdist(d[,c("ht", "stdia","LtoW","area","SLA","PC2","PC3")], method = "euclidean")
## test whether there are differences in dispersion between groups
disp = betadisper(d = dist, group = d$group)
anova(disp)
disp
plot(disp, main = "trait dispersion 2014") #highly sig.

#get average distance to median for each pop
med14 <- cbind(disp$group %>% as.data.frame() %>% rename("pop_ordered" = ".") ,disp$distances %>% as.data.frame() %>% rename("dist_to_median" = ".")) %>% 
  mutate(pop_ordered = gsub("-2014","",pop_ordered)) %>% 
  group_by(pop_ordered) %>% mutate(avg_dist_to_median = mean(dist_to_median)) %>% distinct(pop_ordered, avg_dist_to_median) %>% ungroup() %>% 
  mutate(year = "2014", data = "allhtstdialftrts")

#write csv of disp distances so we don't have to always recalculate them
write.csv(med14,"output_data_files/avg_trait_dispersion_distances_per_pop-2014-noPC4.csv", row.names = F) 


#***************************************************************************************************************
#NEW SECTION: explore relationships btw 2014 disp. and genetic div ----------------------------------------

rm(list = ls())
gc()

#get trait dispersion dists
med <- read.csv("output_data_files/avg_trait_dispersion_distances_per_pop-2014-noPC4.csv", header = T)

#get F-stat data at pop level
gdiv <- read.csv("input_data/summarypopstats.stacks2.1m3max3M4n4.vcf.csv")
head(gdiv)
dim(med)
med <- merge(med, gdiv, by.x = "pop_ordered", by.y = "pop", all.x = T)
dim(med)


#Hs aka genetic diversity vs. trait disp. ---
#outlier pop 4057 rmvd
md <- med %>% filter(pop_ordered != "4057")
fit <- lm(avg_dist_to_median ~ mean.hs.no0, data = md)
Anova(fit)
summary(fit)

#percent fixed SNPs vs trait disp. ---
md <- med
fit <- lm(avg_dist_to_median ~ perc_fixed_snps, data = md)
Anova(fit)
summary(fit)

#check with outlier pop 4057 removed to make compropable to Hs model
md <- med %>% filter(pop_ordered != "4057")
fit <- lm(avg_dist_to_median ~ perc_fixed_snps, data = md)
Anova(fit)
summary(fit)


# FIGURE 5 - dispersion vs fixed SNPs and Hs --------------
med.g <- med %>% 
  dplyr::select(pop_ordered,avg_dist_to_median,perc_fixed_snps,mean.hs.no0) %>%
  pivot_longer(., names_to = "metric", values_to = "value", cols = 3:4) %>% 
  mutate(metric.y = "Mean trait dispersion") %>% filter(pop_ordered != "4057")
med.g.4057 <- med %>% filter(data == "allhtstdialftrts") %>% 
  dplyr::select(pop_ordered,avg_dist_to_median,perc_fixed_snps,mean.hs.no0) %>%
  pivot_longer(., names_to = "metric", values_to = "value", cols = 3:4) %>% 
  mutate(metric.y = "Mean trait dispersion")

med.g$metric[med.g$metric == "mean.hs.no0"] <- "Mean genetic diversity"
med.g$metric[med.g$metric == "perc_fixed_snps"] <- "Percent non-polymorphic SNPs"
med.g.4057$metric[med.g.4057$metric == "mean.hs.no0"] <- "Mean genetic diversity"
med.g.4057$metric[med.g.4057$metric == "perc_fixed_snps"] <- "Percent non-polymorphic SNPs"

#add on R2 value
ann_text1 <- data.frame(value = 0.275,avg_dist_to_median = 0.2285,
                        metric = factor("Mean genetic diversity",levels = c("Mean genetic diversity","Percent non-polymorphic SNPs")),
                        metric.y = factor("Mean trait dispersion",levels = c("Mean trait dispersion")))
#add on P value
ann_text2 <- data.frame(value = 0.2735,avg_dist_to_median = 0.2205,
                        metric = factor("Mean genetic diversity",levels = c("Mean genetic diversity","Percent non-polymorphic SNPs")),
                        metric.y = factor("Mean trait dispersion",levels = c("Mean trait dispersion")))


ggplot(med.g, aes(x=value, y=avg_dist_to_median)) +
  geom_smooth(data = med.g, aes(colour = metric, fill = metric), method="lm", formula = y ~ x, size = 0.7, alpha = 1) +
  scale_colour_manual(values=c("black","white")) +
  geom_point(data = med.g.4057, aes(fill = pop_ordered),size=1.1,shape=21) +
  scale_fill_manual(values=c("black","black","black","black","white","black","black","black","black","black","black","black","grey88","white")) +
  theme_bw() +
  theme(panel.grid = element_blank(),
        axis.title = element_blank(),
        strip.placement = "outside",
        strip.background = element_blank(),
        legend.position="none",
        strip.text = element_text(size=9.5),
        axis.text = element_text(size=8.5)) +
  facet_grid(metric.y ~ metric, scales="free_x", switch = "both") +
  geom_text(data = ann_text1,label = "italic(R)^2 == 0.40", parse = T, size = 2.75) +
  geom_text(data = ann_text2,label = "bolditalic(P)~bold(' < 0.05')", parse = T, size = 2.75) +
  ylim(c(0.22,0.33))

ggsave("figures/figure5-trait_dispersion_to_FISandfixedSNP_plot.pdf",
       width = 127, height = 74, dpi = 600, units = c("mm")) #112

#
