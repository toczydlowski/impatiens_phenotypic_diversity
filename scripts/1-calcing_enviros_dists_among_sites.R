#idea: get environmental data (climate, soils) and calculate enviro dists between field sites and between each field site and common garden
# field sites are characterized by 30 year means including 2014, the year we measured mother plants in the field
# common garden (cg) is characterized by the one year progeny were in the common garden (2015)
# all data were daily values that were sequentially averaged up and all data were from range that progeny were in cg - April 24 to October 15
# which is also the approximate normal growing season for I. capensis in south/central Wisconsin

#   climate with 1985-2015 data (daily)
#   climate with 2014 to 2015 data (daily)
#   soils (my analyses)
#   climate XXX + soil data


#load libraries
library(dplyr)
library(tidyr)


rm(list = ls())
gc()


#***************************************************************************
# NEW SECTION - get soil distances between sites -----------
#***************************************************************************

s.raw <- read.csv("input_data/Site_Data/Soil_Data/soils.csv", header = T)
s.raw <- s.raw %>% dplyr::select(-Name,-RIVER,-WTSHED,-Sand) %>% dplyr::rename(pop=SampleID) #removing sand bc ow sand,silt,clay sum to 100% and mess up stats
#center and scale the data
s.scaled <- s.raw %>% mutate(pH = scale(pH,center = T, scale = T),
                         OM = scale(OM,center = T, scale = T),
                         TN = scale(TN,center = T, scale = T),
                         P = scale(P,center = T, scale = T),
                         K = scale(K,center = T, scale = T),
                         Ca = scale(Ca,center = T, scale = T),
                         Mg = scale(Mg,center = T, scale = T),
                         Silt = scale(Silt,center = T, scale = T),
                         Clay = scale(Clay,center = T, scale = T))
s.raw$pop <- as.character(s.raw$pop)
s.raw$pop[as.character(s.raw$pop) == "UWArb"] = "Arb"
#put field data and common garden data wide, side by side
s.f <- s.scaled %>% filter(pop != "UWArb")
s.cg <- s.scaled %>% filter(pop == "UWArb") 
s.cg <- s.cg %>% 
  dplyr::rename(pH.cg = pH, OM.cg = OM, TN.cg = TN, P.cg = P, K.cg = K, Ca.cg = Ca, Mg.cg = Mg, Silt.cg = Silt, Clay.cg = Clay) %>% 
  dplyr::select(-pop)
s.f
s.cg
s <- merge(s.f, s.cg, all.x = T)
s




#***************************************************************************
# NEW SECTION - get light distances between sites -----------
#***************************************************************************

# Do not have canopy photo (proxy for light levels in site) for the Arb : (or 4 of the 12 field sites)

#get canopy openess/light data (only have for 8 field sites)
light <- read.delim("input_data/Site_Data/Canopy_Data/Canopy_analysis_in_GLA.txt", sep = ";") %>% 
  dplyr::select(User.Field.1,X..Cnpy.Open) %>% rename("pop" = "User.Field.1", "perc_open_sky" = "X..Cnpy.Open") %>% 
  mutate_all(.,stringr::str_trim) %>% mutate(perc_open_sky = as.numeric(perc_open_sky))




#***************************************************************************
#NEW SECTION - get climate data -----------
#***************************************************************************

#***************************************************************************
#PROGENY, 4/24/15-10/15/15, common garden location -------

#getting data for progeny plants for 2015 for period of time they were in the common garden aka growing season
#this way, we don't have to use greenhouse estimated vpd, weird ppt values, etc.
#get data for 1 yr plants were in Arb
#4/24/15 - moved plants to Arb (get PRISM data for Arb 4km chunk for 4/24/15-10/15/15)
c = data.frame(date = NA, ppt=NA, tmin=NA, tmean=NA, tmax=NA, vpdmin=NA, vpdmax=NA, pop=NA)
climatefiles <- list.files("input_data/Site_Data/Climate_Data/2014-2015data_perday_cg/", pattern=".csv", all.files=FALSE,
                           full.names=T)

for(i in climatefiles) {
  ctemp <- read.delim(i, skip = 11, header = F) %>% 
    separate(.,V1, into = c("date","ppt","tmin","tmean","tmax","vpdmin","vpdmax"),sep = ",") 
  pop <- regexpr("[A-z0-9+]{2,}-PRISM", i)
  pop <- regmatches(i, pop)
  pop <- gsub(pop, pattern = "-PRISM", replacement = "")
  ctemp <- ctemp %>% mutate(pop = pop)
  c = rbind(c, ctemp)  
  c <- c %>% na.omit()
}

c$pop <- as.factor(c$pop)
c$ppt <- as.numeric(c$ppt)
c$tmin <- as.numeric(c$tmin)
c$tmean <- as.numeric(c$tmean)
c$tmax <- as.numeric(c$tmax)
c$vpdmin <- as.numeric(c$vpdmin)
c$vpdmax <- as.numeric(c$vpdmax)

c <- separate(c,date,into = c("y","m","d"),sep = "-")

#keep days plants were in Arb
#filter out 2013 and 2014 data (we only want 2015)
#filter out Jan, Feb, March, April 1-23, remaining data is for the days that the plants were in the Arb 
c <- c %>% filter(y == "2015") %>% filter(m != "01") %>% filter(m != "02") %>% filter(m != "03")
c <- c[-1:-23,]
c.fieldtime <- c
c.fieldtime <- c.fieldtime %>% group_by(y,m) %>% 
  mutate(ppt.t = sum(ppt)) %>% mutate(tmin.avg = mean(tmin)) %>% mutate(tmean.avg = mean(tmean)) %>%
  mutate(tmax.avg = mean(tmax)) %>% mutate(vpdmin.avg = mean(vpdmin)) %>% mutate(vpdmax.avg = mean(vpdmax)) %>% 
  dplyr::select(-ppt,-tmin,-tmean,-tmax,-vpdmin,-vpdmax, -d) %>% 
  unique() %>% 
  group_by(y) %>%
  mutate(ppt_fieldtime = sum(ppt.t), tmin_fieldtime = mean(tmin.avg), 
                                          tmean_fieldtime = mean(tmean.avg), tmax_fieldtime = mean(tmax.avg), 
                                          vpdmin_fieldtime = mean(vpdmin.avg), vpdmax_fieldtime = mean(vpdmax.avg)) %>% 
  ungroup() %>% 
  dplyr::select(pop,contains("field")) %>% unique() %>% as.data.frame()
c.fieldtime



#***************************************************************************
#MOTHERS, 4/24/XXXX - 10/15/XXXX for 1985-2014, field site locations -------

c = data.frame(date = NA, ppt=NA, tmin=NA, tmean=NA, tmax=NA, vpdmin=NA, vpdmax=NA, pop=NA)
climatefiles <- list.files("input_data/Site_Data/Climate_Data/1985-2016data_perday_fieldsites/", pattern=".csv", all.files=FALSE,
                           full.names=T)

for(i in climatefiles) {
  ctemp <- read.delim(i, skip = 11, header = F) %>% 
    separate(.,V1, into = c("date","ppt","tmin","tmean","tmax","vpdmin","vpdmax"),sep = ",") 
  pop <- regexpr("[A-z0-9+]{2,}-PRISM", i)
  pop <- regmatches(i, pop)
  pop <- gsub(pop, pattern = "-PRISM", replacement = "")
  ctemp <- ctemp %>% mutate(pop = pop)
  c = rbind(c, ctemp)  
  c <- c %>% na.omit()
}

c$pop <- as.factor(c$pop)
c$ppt <- as.numeric(c$ppt)
c$tmin <- as.numeric(c$tmin)
c$tmean <- as.numeric(c$tmean)
c$tmax <- as.numeric(c$tmax)
c$vpdmin <- as.numeric(c$vpdmin)
c$vpdmax <- as.numeric(c$vpdmax)

c <- separate(c,date,into = c("y","m","d"),sep = "-") %>% mutate(m_d = paste(m,d,sep="_"))
#should be 11688 lines data * 12 pops = 140256 rows

#filter out 2015 data (mother plants died in 2014)
c <- c %>% filter(y != "2015")
#filter out Jan, Feb, March, Nov, Dec
c <- c %>% filter(m != "01") %>% filter(m != "02") %>% filter(m != "03") %>% filter(m != "11") %>% filter(m != "12")
#filter out April 1-23
c <- c %>% filter(!m_d %in% c("04_01","04_02","04_03","04_04","04_05","04_06","04_07","04_08","04_09","04_10","04_11",
                              "04_12","04_13","04_14","04_15","04_16","04_17","04_18","04_19","04_20","04_21","04_22","04_23"))
#filter out Oct 16-31
c <- c %>% filter(!m_d %in% c("10_16","10_17","10_18","10_19","10_20","10_21","10_22","10_23","10_24","10_25","10_26",
                           "10_27","10_28","10_29","10_30","10_31"))
  #175 days per each year/pop combo

c.avg <- c %>% group_by(y,pop) %>% 
  mutate(ppt.t = sum(ppt)) %>% mutate(tmin.avg = mean(tmin)) %>% mutate(tmean.avg = mean(tmean)) %>%
  mutate(tmax.avg = mean(tmax)) %>% mutate(vpdmin.avg = mean(vpdmin)) %>% mutate(vpdmax.avg = mean(vpdmax)) %>%
  as.data.frame() %>% 
  dplyr::select(pop,y,ppt.t,tmin.avg,tmean.avg,tmax.avg,vpdmin.avg,vpdmax.avg) %>% unique()
  #should be 31 yrs x 12 sites = 372

#have values for each yr/pop combo, now get 1 avg value for each site across all 31 yrs
c.f.30.fieldtime <- c.avg %>% group_by(pop) %>% summarise(ppt_fieldtime = mean(ppt.t), 
                                                tmin_fieldtime = mean(tmin.avg),
                                                tmean_fieldtime = mean(tmean.avg),
                                                tmax_fieldtime = mean(tmax.avg),
                                                vpdmin_fieldtime = mean(vpdmin.avg),
                                                vpdmax_fieldtime = mean(vpdmax.avg)
                                                ) %>% as.data.frame()
c.f.30.fieldtime


#***************************************************************************
#join 1 yr of 2015 common garden data and 30yrs of field data and center and scale

c.f.30.fieldtime
c.fieldtime
c.field <- rbind(c.f.30.fieldtime,c.fieldtime)
c.field

c.field <- c.field %>% mutate(ppt_fieldtime = scale(ppt_fieldtime, center = T, scale = T),
                              tmin_fieldtime = scale(tmin_fieldtime, center = T, scale = T),
                              tmean_fieldtime = scale(tmean_fieldtime, center = T, scale = T),
                              tmax_fieldtime = scale(tmax_fieldtime, center = T, scale = T),
                              vpdmin_fieldtime = scale(vpdmin_fieldtime, center = T, scale = T),
                              vpdmax_fieldtime = scale(vpdmax_fieldtime, center = T, scale = T))
c.field

#split back to field data side by side with common garden data
c.field.f <- c.field %>% filter(pop != "Arb") %>% rename(ppt_30yrfieldtime = ppt_fieldtime,
                                                         tmin_30yrfieldtime = tmin_fieldtime,
                                                         tmean_30yrfieldtime = tmean_fieldtime,
                                                         tmax_30yrfieldtime = tmax_fieldtime,
                                                         vpdmin_30yrfieldtime = vpdmin_fieldtime,
                                                         vpdmax_30yrfieldtime = vpdmax_fieldtime)

c.field.cg <- c.field %>% filter(pop == "Arb") %>% rename(ppt_fieldtime.cg = ppt_fieldtime,
                                                         tmin_fieldtime.cg = tmin_fieldtime,
                                                         tmean_fieldtime.cg = tmean_fieldtime,
                                                         tmax_fieldtime.cg = tmax_fieldtime,
                                                         vpdmin_fieldtime.cg = vpdmin_fieldtime,
                                                         vpdmax_fieldtime.cg = vpdmax_fieldtime) %>% 
  dplyr::select(-pop)

c.field.f
c.field.cg
c.field <- merge(c.field.f, c.field.cg, all.x = T)
c.field



# merge all enviro data together ------------
s #soils from common garden and field sites - already centered and scaled
c.field #climate from common garden and field sites - already centered and scaled
e <- merge(s,c.field,by="pop")

#write out final files - all enviro data and enviro dists ----
write.csv(merge(merge(s.raw,rbind(c.f.30.fieldtime,c.fieldtime), by = "pop"),light, all.x = T, by = "pop"),"output_data_files/enviro_site_data-notscaled.csv", row.names = F)
write.csv(e,"output_data_files/enviro_site_data.csv", row.names = F)




#***************************************************************************
# NEW SECTION - CALC ENVIRO DISTS BETWEEN FIELD SITES ------------------
#***************************************************************************
rm(list = ls())
gc()

#get 30 yr averages for field sites (and filter out the Arb data - just for the time plants were in the common garden)
yr.30 <- read.csv("output_data_files/enviro_site_data-notscaled.csv", header = T) %>% filter(pop != "Arb") %>% dplyr::select(-perc_open_sky)

#get long list of all enviro data
yr.30.l <- yr.30 %>% gather(.,"variable","value",2:ncol(yr.30)) 

#scale data within each enviro variable (and with Arb removed)
yr.30.l.s <- yr.30.l %>% group_by(variable) %>% mutate(value.s = scale(value, center = T, scale = T)) %>% ungroup(.)

#get all pairwise combinations of sites so for each of 15 enviro variables we have all 66 pairwise combos of field sites
df <- yr.30.l.s %>% mutate(linked = paste(pop,variable,value.s,sep = ",")) %>% dplyr::select(linked)
df <- combn(df$linked,2) %>% t() %>% as.data.frame() %>% separate(.,V1,into = c("pop1","var1","value1"),sep = ",") %>% 
  separate(.,V2,into = c("pop2","var2","value2"),sep = ",") %>% mutate(keep = ifelse(var1 == var2,1,0)) %>%
  filter(keep == 1) %>% dplyr::select(-keep,-var2) %>% rename(variable = var1)
df$value1 <- as.numeric(df$value1)
df$value2 <- as.numeric(df$value2)

#get distance for each enviro var and pop pair
df <- df %>% mutate(poppair = paste(pop1,pop2,sep = ",")) %>% mutate(dist = (value1 - value2)^2)

#convert dataframe back to wide
df.w <- df %>% dplyr::select(poppair,variable,dist) %>% spread(.,variable,dist)
df.w.enviro <- df.w %>% mutate(enviro30yrdist = sqrt(Ca + Clay + K + Mg + OM + P + pH + ppt_fieldtime + Silt +
                                                tmax_fieldtime + tmean_fieldtime + tmin_fieldtime + TN +
                                              vpdmax_fieldtime + vpdmin_fieldtime))

#write out final dataframe here (do modeling in a separate script to keep things cleaner)
write.csv(df.w.enviro, "output_data_files/enviro_distances_btw_fieldsites_30yravgs.csv", row.names = F)




#***************************************************************************
# NEW SECTION - CALC ENVIRO DISTS BETWEEN FIELD SITES AND CG ------------------
#***************************************************************************
rm(list = ls())
gc()

e <- read.csv("output_data_files/enviro_site_data.csv", header = T)

e$soils_dist_f_cg = with(e, sqrt((pH-pH.cg)^2 + 
                                        (OM-OM.cg)^2 + 
                                        (TN-TN.cg)^2 + 
                                        (P-P.cg)^2 + 
                                        (K-K.cg)^2 + 
                                        (Ca-Ca.cg)^2 +
                                        (Mg-Mg.cg)^2 +
                                        (Silt-Silt.cg)^2 +
                                        (Clay-Clay.cg)^2))

e$climate_dist_f30yrfieldtime_cgfieldtime = with(e, sqrt((ppt_30yrfieldtime-ppt_fieldtime.cg)^2 + 
                                                           (tmin_30yrfieldtime-tmin_fieldtime.cg)^2 + 
                                                           (tmean_30yrfieldtime-tmean_fieldtime.cg)^2 + 
                                                           (tmax_30yrfieldtime-tmax_fieldtime.cg)^2 +
                                                           (vpdmax_30yrfieldtime-vpdmax_fieldtime.cg)^2 + 
                                                           (vpdmin_30yrfieldtime-vpdmin_fieldtime.cg)^2
                                                         ))

e$enviro_dist_f30yrfieldtime_cgfieldtime = with(e, sqrt((ppt_30yrfieldtime-ppt_fieldtime.cg)^2 + 
                                                   (tmin_30yrfieldtime-tmin_fieldtime.cg)^2 + 
                                                   (tmean_30yrfieldtime-tmean_fieldtime.cg)^2 +
                                                   (tmax_30yrfieldtime-tmax_fieldtime.cg)^2 +
                                                   (vpdmax_30yrfieldtime-vpdmax_fieldtime.cg)^2 + 
                                                   (vpdmin_30yrfieldtime-vpdmin_fieldtime.cg)^2 +
                                                  (pH-pH.cg)^2 + 
                                                  (OM-OM.cg)^2 + 
                                                  (TN-TN.cg)^2 + 
                                                  (P-P.cg)^2 + 
                                                  (K-K.cg)^2 + 
                                                  (Ca-Ca.cg)^2 +
                                                  (Mg-Mg.cg)^2 +
                                                  (Silt-Silt.cg)^2 +
                                                  (Clay-Clay.cg)^2))

e$temp_dist_f30yrfieldtime_cgfieldtime = with(e, sqrt( 
                                                   (tmin_30yrfieldtime-tmin_fieldtime.cg)^2 + 
                                                   (tmean_30yrfieldtime-tmean_fieldtime.cg)^2 +
                                                   (tmax_30yrfieldtime-tmax_fieldtime.cg)^2 ))
                                              
e$vpd_dist_f30yrfieldtime_cgfieldtime = with(e, sqrt(
                                                        (vpdmax_30yrfieldtime-vpdmax_fieldtime.cg)^2 + 
                                                        (vpdmin_30yrfieldtime-vpdmin_fieldtime.cg)^2))

e$ppt_dist_f30yrfieldtime_cgfieldtime = with(e, sqrt((ppt_30yrfieldtime-ppt_fieldtime.cg)^2))

#select final distances
e.dists <- e %>% dplyr::select(pop,
                        soils_dist_f_cg,
                        climate_dist_f30yrfieldtime_cgfieldtime,
                        enviro_dist_f30yrfieldtime_cgfieldtime,
                        temp_dist_f30yrfieldtime_cgfieldtime,
                        vpd_dist_f30yrfieldtime_cgfieldtime,
                        ppt_dist_f30yrfieldtime_cgfieldtime)


#write out final files - all enviro data and enviro dists ----
write.csv(e.dists, "output_data_files/enviro_distances_btw_commongarden_and_fieldsites.csv", row.names = FALSE)




