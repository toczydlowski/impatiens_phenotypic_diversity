#README

INPUT DATA



Overall notes - 

The populations in the Green Bay area were not along one specific river but a few smaller rivers, so we coded RIVER for them as Gbay or GreenBay, but this does not refer to a specific river like the other levels of the RIVER variable do (Rock, Sugar, Milwaukee).

Missing values are represented by NA or an empty cell.



------------------------------------------------------------------------------------------------------------
File: cg_2015_data-8-1-17.csv

Description: Morphological and reproductive data for progeny plants growing in the common garden in 2015. These progeny are derived from cleistogamous seeds collected from a subset of the mother plants measured in remote home field sites in 2014.

Protocol details: Impatiens capensis cleistogamous seeds were collected from August-October 2014 at 12 sites in S/Central Wisconsin from mother plants (measured in 2014 dataset). Stored one pod/seed capsule per tiny ziplock bag labeled with date, CL seed, and field plant ID number of mother plant. Kept the seeds in sealed bags in a cooler (but no ice) and brought them back to the lab and put them in the fridge. As had time (but always tried for within a week of collection), opened each bag, weighed each seed individually on a microbalance, and put each seed into a well of a flat bottom tissue culture plate with DI water (seeds need to stay wet to remain good viability). Seeds were moved to Percival growth chamber at 0-5C in the dark over the winter. Seeds were planted into individual “pots” in standard black plastic flats (72 “pots” per flat) in the UW Biotron G127 room.  ProMix BX soil was used.  Biotron was kept at ~5C with natural light and plants were watered on ebb and flow table in morning at evening for 8-12min each time.  Seedlings were moved out to the UW Arboretum field site (Arb) and planted directly into the ground by hand during the first week of March 2015.  Ground was cleared of large logs and then raked to clear smaller debris.  No herbicides were applied.  Soil on the site was open, wet, and mucky with no vegetation present when seedlings were planted.   

Plot in Arb. split into 3 blocks - Block 1 was located closest to Teal pond (and furthest from fire road) and Block 3 was located closest to fire lane.  All blocks ran parallel to the fire road.  Plants were planted in rows on a 10x10cm grid within each block.  See miscellaneous_additional_information.pdf for further details about plant ID numbering pattern.  Plant #1 was planted first and plants were planted in numerical order after.  Some plants were lost to multiple heavy and unexpected frosts and deer herbivory during the week of planting.  Deer fence was installed around the site in early March to prevent further herbivory (no further deer sign or herbivory after fence installed). Plants were split equally across blocks by field site, mother plant, and seed type. Seeds were planted in a randomized design in the Biotron and then flats were planted in a random order in the field to minimize any greenhouse effects.

6/30/15 – 7/2/15 (Field Measurement 1) – First set of field measurements on all experimental plants out in the Arb.  Measured height of plant from ground to apex of plant (to nearest 0.5cm) using a meter stick.  Measured diameter of stem using digital calipers at base of plant.  Counted total number of CH and CL reproductive parts separately on each plant.  Within CH and CL parts, did not differentiate between buds, open flowers, seed pods, etc. and lumped all of these structures together in the reproductive count.  Recorded the presence/absence of any flower galls.  Some plants had bright orange fungus-looking material on the stem and the stem was swollen in this area.  Recorded this as “stem disease”.  Also recorded herbivory from deer (before the fence was erected but still obvious on the plants as the tops were clipped off).

7/21/15 – 7/25/15 (Field Measurement 2) – Started second round of measurements for the year on the Arb field site.  Height, stem diameter, and gall/disease measurements taken in same way as during first measurements.  From this point forward, only mature seed capsules (or those already exploded with the pedicle remaining) were counted.  This was done to more accurately capture fitness (producing a seed) and to avoid double-counting a reproductive structure from one set of measurements to the next.  Pedicles that were counted were pinched off using fingernails to avoid counting them again in future counts.  

8/11/15 – 8/16/15 (Field Measurement 3) – Same procedure as round 2 measurements.

8/30/15 – 9/3/15 (Field Measurement 4) – Same procedure as round 2 measurements.

9/26/15 (Field Measurement 5) – Same procedure as round 2 measurements except did not measure height or stem diameter as plants were done growing for the season (and only producing reproductive parts at this point).

10/14/15 (Field Measurement 6) – Same procedure as round 5 measurements.  All plants were already dead at this point or almost dead with seed pods (no new buds/flowers present).

Height and stem diameter data were recorded by voice recording on an iPhone and automatically dictated into Excel using talk to text Dictation in System Preferences in OS X. Dictated data were checked by listening to the original audio file and visually confirming the entered value. Reproductive counts, disease data, and miscellaneous comments were recorded by hand onto paper datasheets in the field and entered manually into Excel. Manually entered data were visually spot checked and outlier values (as visually identified on scatterplots or histograms) were checked against the original paper copy. All edits and additions to the dataset were made in a new copy of the dataset (save as).

Plants were watered by hand with jugs a few times during the experiment to help seedlings recover from transplant shock and to prevent massive die-offs during a few especially/uncharacteristically long dry spells. Most plant deaths throughout the season seemed to be due to lack of water.

Site location: 
Arboretum designation: Teal Pond Wetland Area (W4)

Coordinates: 43° 02’ 17” N, 89° 25’ 29” W

Verbal description: If walking around W4 on fire lane clockwise, plot is between the pond berm and fire lane after the cement culvert and before the trail branches and heads back around W4 towards the Visitor Center.

Columns names – description; units:

FIELDSITE – Name of remote home field site plant originated from; unitless
RIVER - Name of river system field site is in/near; unitless
WTSHED - Name of greater watershed that field site is in, GL for Great Lakes watershed and MI for Mississippi watershed; unitless
MomID.2014 – Unique ID of the mother plant in each field site that this progeny originated from; unitless
date.seedcollected - Date seed collected from mother plant in remote home field site; m/dd/yy
seed.type - Denotes seeds are cleistogamous seeds (CL); text
pod.num	- Numerical ID of the collected seed pod that progeny originated from, numbered within each mother; integer
seed.num.inpod - Total number of seeds present in the collected seed pod; integer 
seed.mass.g - Seed mass; grams
Comments.SeedWeighing - Comments from the ziplock baggies or about the quality of the seeds as I weighed them or mishaps when weighing (like loosing seeds); unstructured text
Comments.GrowthChamber - Comments from the time seeds were in well plates over winter in the growth chamber; unstructured text
Germinated.in.plate.pre.planting.YN - Binary y/n variable where y means the seed germinated will still in the well plate and n means the seed had not germinated when it was planted into soil in the greenhouse; binary (y/n)
LINK.plate - Synthetic variable of form Plate-Plate.Row-Plate.Column; factor
Plate - Plate ID number that seed was stored in in growth chamber; factor
Plate.Row - Plate row that seed was stored in in growth chamber; factor
Plate.Column - Plate column that seed was stored in in growth chamber; factor
LINK.flats - Synthetic variable of form FlatFlat.RowFlat.Column; factor
Flat - Flat ID number that seed was planted into in greenhouse; factor
Flat.Row - Row of germination flat where seed was planted; factor
Flat.Column - Column of germination flat where seed was planted; factor
BLOCK.grh - Block number that flat was in in greenhouse (one of three tables); factor
Comments.seedplanting - Comments written while planting seeds into greenhouse from well plates; unstructured text
seed.issues - Categorical variable where none means no issues, dry means water level was very low in well plate cell, slimy.seed means water in well plate cell was cloudy/thick, and slimy.plate means water for whole well plate was overall cloudy/thick; factor
germ.date - Date that germination was noticed, checked germination about once per week; m/dd/yy
germinated.YN - Binary y/n variable where y means seed germinated and n means seed did not germinate; binary (y/n)
Comments.GerminationChecking - Comments from weekly checks for germination; unstructured text
seedling.ht.cm - Seedling height from soil surface to top of seedling as it stood naturally; nearest cm
seedling.width.mm - Seedling width from apex to apex of the cotyledons when held parallel to soil, measured using digital calipers; mm
seedling.mort.greenhouse - Binary variable for if plants were alive during seedling measurements or dead, only recorded for seeds that germinated; binary (alive/dead)
seedling.mildew - Binary variable of y if mildew noticed on seedling leaves and n if no leaf disease noticed, only recorded for seeds that germinated; binary (y/n)
Comments.InitialSeedlingMeasurements - Comments recorded while measuring seedling height and width; unstructured text
Transplanting.Order - Order that flats of seedlings were transplanted from the greenhouse out into the common garden; integer
frost.mort - Binary variable where alive means seedling was in flat in common garden and exposed to one or both of heavy frosts during transplanting and survived, dead means seedling was in flat and exposed to frost and died as a result, or blank or NA for seedlings that were not exposed to frost; binary (alive/dead)
BLOCK.Arb - Block ID that plant was in in the common garden; factor
Row.Arb - Row ID that plant was in in the common garden, factor
POS.Arb - Unique ID that plant was given in the common garden, factor
seedling.issue - Binary variable where yes means seedling had a deformed apical meristem and/or loss of chlorophyll and no means seedling appeared normal; binary (yes/no)
seedling.sad - Binary variable where yes means seedling had a seedling issue and/or mildew and no means seedling appeared normally formed and not diseased; binary (yes/no)
Date.FieldMeasurements.X - Date that plants were measured in the common garden, 6 rounds of measurements total where 1 is the first time plants were measured; m/dd/yy
FieldMesureX.Height.cm - Plant height for measurement round X; cm
FieldMeasure1.Total.Repro - Total number of reproductive structures present on plant for measurement round 1; count
FieldMeasure1.CH.repro - Total number of chasmogamous reproductive structures present on plant for measurement round 1; count
FieldMeasure1.CL.repro - Total number of cleistogamous reproductive structures present on plant for measurement round 1; count
FieldMeasureX.Flower.gall.Y.N - Binary variable where y means at least one flower gall noticed on plant and n means no flower galls noticed for measurement round X; binary (y/n)
FieldMeasure1.Top.gone.Y.N - Binary variable where y means top of plant clipped off by deer (before deer fence was installed) and n means no deer herbivory; binary (y/n)
FieldMeasureX.Leaf.gall.Y.N - Binary variable where y means at least one leaf gall noticed on plant and n means no leaf galls noticed for measurement round X; binary (y/n)
FieldMeasure1.Stem.disease.Y.N - Binary variable where y means swollen orange fungus growth noticed on plant stem and n means no growth noticed for measurement round X; binary (y/n)
FieldMeasureX.Mortality - Binary variable where alive means plant was alive during round X of measurements and dead means plant was dead; binary (alive/dead)
Comments.FieldMeasurements.X - Comments recorded during round X of common garden measurements; unstructured text
FieldMeasureX.CL.mature.repro - Total number of mature cleistogamous reproductive structures on plant - ripe seed capsules and bare pedicles - during measurement round X; count
FieldMeasure2.CH.mature.repro - Total number of mature chasmogamous reproductive structures on plant - ripe seed capsules and bare pedicles - during measurement round X; count
fX.mort.newdeaths - Binary variable where alive means plant was alive during round X of measurements and dead means plant died between last round of measurements and this round; binary (alive/dead)
cg.stamina - Ordinal variable representing which field measurement round plant survived until, 0 means died between transplanting and first measurement round, 1 means died between first and second measurement rounds, 6 means still alive at final round of measurements; ordinal
Comments.final.corrections.made.to.field.data.based.on.blue.ID.tags.collected.from.the.field.at.end.of.year - Comments about corrections that were made to field measurement data at the end of year after collecting plant ID tags from the common garden site; unstructured text
FINAL.Height - Maximum height measured for each plant in the common garden; cm	
FINAL.StemDia - Maximum stem diameter measured for each plant in the common garden; cm	
FINAL.CL..sans..1. - Total number of mature cleistogamous reproductive structures counted in lifetime of plant excluding measurement round 1; count
FINAL.CH..sans..1. - Total number of mature chasmogamous reproductive structures counted in lifetime of plant excluding measurement round 1; count
FINAL.CL...1.6. - Total number of cleistogamous reproductive structures counted in lifetime of plant including measurement round 1; count
FINAL.CH...1.6. - Total number of chasmogamous reproductive structures counted in lifetime of plant including measurement round 1; count
FINAL.herbivory	- Binary variable where y means top of plant clipped off by deer and n means plant did not experience deer herbivory; binary (y/n)
FINAL.flowergall - Binary variable where y means at least one flower gall was noticed during lifetime of plant n means no flower galls were noticed during lifetime of plant; binary (y/n)
FINAL.leafgall - Binary variable where y means at least one leaf gall was noticed during lifetime of plant n means no leaf galls were noticed during lifetime of plant; binary (y/n)
FINAL.stemdisease - Binary variable where y means orange fungal stem growth was noticed during lifetime of plant n means no growth noticed during lifetime of plant; binary (y/n)	
made.ch - Binary variable where y means plant produced at least some mature chasmogamous seeds and n means plant did not; binary (y/n)

------------------------------------------------------------------------------------------------------------
File: field_2014_morphologicaldata-3-22-16.csv

Description: Morphological and reproductive data collected on mother plants (96 per field site) in 2014 in 12 remote field sites in S/C WI. Data were written on paper datasheets in the field, entered manually into Excel, and proofed for errors.

Protocol details: Sampled 96 plants in each of 12 remote home field sites. Placed a temporary plot of 10x1.5m in each site. A temporary yellow stake marked one corner of this plot that was (0,0) and we used a measuring tape to locate the plant closest to each (x,y) random position (positions generated to the nearest 1 cm). Randomized numbers were not followed in pops 4033, 4057, 4079, FW, 4029,and ALB because there were too few plants in the plot to follow the grid and have it be accurate. Instead we picked plants randomly and/or closest to the X,Y grid location that we should have sampled and the positions columns are therefore blank for those populations (and the location of the plant is not known - similar plant ID numbers were generally close to each other in space but not always). Flower galls are cases where the flower bud was turned into a gall by some sort of insect. Leaf galls were a bump formed out of a portion of the leaf midrib/main vein, often near the petiole. We found small wiggly larvae inside some of the galls that we opened. We did not record gall data for all populations, so a blank or NA means that we did not score this variable in that population (we did not see galls until part way through the sampling and did not get a chance to go back to all populations and record this). Wilt refers to some sort of disease that caused the leaves and/or stems/branches to be black, limp, and squishy. Chewed variable captures major herbivory aka branches and/or apical meristem clipped off. We did not focus on individual leaves being chewed by insects or not. Plant height was measured using a metric cloth/soft sewing tape stapled to a piece of wooden trim (so we could measure more than 1 m at a time), measured from the ground to the tip of the tallest point on the plant as it stood naturally; we did not pull leaves up to get this measurement but did straighten falling over/leaning plants). Stem diameter was measured to the tenth of a cm using handheld digital calipers at the base of each plant just above the sharp buttress at the plant base where prop roots come out; that is, we did not measure on the buttress/tapered region, but just above it, where the stem was not noticeably tapered. Note: ch.flw.no - ch.openflower.no - ch.buds.no = the number of small (green) chasmogamous buds on the plant on the day we measured. 


Columns names – description; units:

FIELDSITE – Name of remote home field site; unitless
RIVER - Name of river system field site is in/near; unitless
WTSHED - Name of greater watershed that field site is in, GL for Great Lakes watershed and MI for Mississippi watershed; unitless
latitude - Latitude of the (0,0) corner of the sampling plot collected with a handheld Garmin GPS in the field that had 3-5 m accuracy when measurements were taken; decimal degrees
longitude - Longitude of the (0,0) corner of the sampling plot collected with a handheld Garmin GPS in the field that had 3-5 m accuracy when measurements were taken; decimal degrees
MomID.2014 – Unique ID of the mother plant in each field site; unitless
Date.Measured - Date that morphological and reproductive measurements were collected; m/dd/yy
ht - Plant height; nearest cm
stdia - Stem diameter; cm
cl.flw.no - Total number of cleistogamous structures that were on the plant on the day that we measured that were a small bud or a flower; count
cl.seedcap.no - Total number of cleistogamous structures that were on the plant on the day that we measured that were a visible seed capsule through a pedicle left where a seed capsule had previously exploded; count
ch.buds.no - Total number of chasmogamous flowers that were large, developed flowers (with a spur) that were close to opening but not yet open on the day we measured the plant, this number is a subset of ch.flw.no variable; count
ch.openflower.no - Total number of chasmogamous flowers that were open on the day we measured the plant, this number is a subset of ch.flw.no variable; count
ch.flw.no - Total number of chasmogamous structures that were on the plant on the day that we measured that were a small bud through an open flower; count
ch.seedcap.no - Total number of chasmogamous structures that were on the plant on the day that we measured that were a small seed capsule (just after a flower had fallen off) through a pedicle left where a seed capsule had previously exploded; count
Chewed.Y.N - Binary y/n variable that is n for plant did not show any signs of deer herbivory on day measured and y for evidence of deer herbivory (aka branches and/or apical meristem clipped off); binary (y,n)
Flower.Galls.Y.N - Binary y/n variable that is n for plants that did not have any flower galls on them and y for plants that had at least one flower gall; binary (y/n)
Leaf.Galls.Y.N - Binary y/n variable that is n for plants that did not have any leaf galls on them and y for plants that had at least one leaf gall; binary (y/n)
Wilt.Y.N - Binary y/n variable representing if the plant had wilt disease (y) or not (n); binary (y/n)
comments.fieldmeasurements - Comments about plants in the field as we were measuring them; unstructured text
pos.x.10mside - Position to the nearest cm of the plant in the plot from the zero corner down the 10m long side of the plot; integer
pos.y.1.5mside - Position to the nearest cm of the plant in the plot from the zero corner down the 1.5m long side of the plot; integer

------------------------------------------------------------------------------------------------------------
File: summarypopstats.stacks2.1m3max3M4n4.vcf.csv

Description: Mean genetic diversity (relative pi) of mothers in each of the 12 sampled populations and the percent of single nucleotide polymorphims fixed in each population.

Protocol details: See Toczydlowski, R. H., & Waller, D. M. (2019). Drift happens: Molecular genetic diversity and differentiation among populations of jewelweed (Impatiens capensis Meerb.) reflect fragmentation of floodplain forests. Molecular ecology, 28: 2459-2475. for details. This is where data were generated and initially analyzed.

Columns names – description; units:
pop - Field Site ID where the sampled individuals originated from
mean.hs.no0 - Mean genetic diversity (pi at polymorphic sites)
stdv.hs.no0 - Standard deviation around mean.hs.no0; unitless
perc_fixed_snps - Percentage of SNPs variant in the metapopulation but fixed within this individual population; percentage

------------------------------------------------------------------------------------------------------------
File: pw_fst_nei-populations.snps-stacks2.1.m3max3M4n4.vcf.Robj

Description: An R object file that contains a matrix of pairwise Fst between all 12 sampled populations (genetic data from a subset of the mother plants measured and reported on in this paper).

Protocol details: See Toczydlowski, R. H., & Waller, D. M. (2019). Drift happens: Molecular genetic diversity and differentiation among populations of jewelweed (Impatiens capensis Meerb.) reflect fragmentation of floodplain forests. Molecular ecology, 28: 2459-2475. for details. This is where data were generated and initially analyzed.


------------------------------------------------------------------------------------------------------------
Folder: Leaf_Data

Description: Original .tif scans of leaves from progeny plants and mother plants plus derived files and data. See README inside folder for details.

------------------------------------------------------------------------------------------------------------
Folder: Site_Data

Description: All environmental data for the 12 remote home field sites and the common garden used to calculate environmental distances. See README inside folder for details.

------------------------------------------------------------------------------------------------------------
- End of file -
