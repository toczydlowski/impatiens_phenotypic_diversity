#README

LEAF DATA

NOTE - these raw data only housed on Dryad and not BitBucket due to space limitations.

------------------------------------------------------------------------------------------------------------
File: Arb2015-leaf_masses.csv

Description: Masses of dried leaves of progeny plants growing in the common garden in 2015.

Protocol details: Leaves were weighed individually on a microbalance in grams.

Columns names – description; units:

POS.Arb – Unique ID in the common garden of the progeny plant leaf collected from; unitless
Leaf.weight.g – Dried mass of the leaf; grams

------------------------------------------------------------------------------------------------------------
File: Field2014-leaf_masses.csv

Description: Masses of dried leaves of wild mother plants growing in the remote home field sites in 2014.

Protocol details: Leaves were weighed individually on a microbalance in grams.

Columns names – description; units:

FIELDSITE – Name of remote home field site; unitless
MomID.2014 – Unique ID of the mother plant leaf collected from; unitless
Leaf.weight.g – Dried mass of the leaf; grams

------------------------------------------------------------------------------------------------------------
Folder: All_useable_processed_scans-Arb2015-rotatedandflipped

Description: Binary black and white leaf scans used to calculate leaf shape and size traits. Leaves are from progeny grown in the common garden in 2015. File names correspond to the unique plant ID position number assigned to plants in the common garden (POS.Arb). Groups_file.csv is a file with names of all leaf scans and the field site, river system, and greater watershed that the progeny derived from. 379 files.

Protocol details: We converted original leaf scans into binary black and white images using FIJI (Schindelin et al. 2012). We manually flipped and/or rotated leaves to standardize the direction of leaf shape asymmetries.

------------------------------------------------------------------------------------------------------------
Folder: All_useable_processed_scans-Field2014-rotatedandflipped

Description: Binary black and white leaf scans used to calculate leaf shape and size traits. Leaves are from wild mother plants growing in the remote home field sites in 2014. File names are FIELDSITE_MomID.2014 aka the field site ID and the unique ID assigned to each measured plant within the temporary plot in each field site. Groups_file.csv is a file with names of all leaf scans and the field site, river system, and greater watershed that they were sampled from. 835 files.

Protocol details: We converted original leaf scans into binary black and white images using FIJI (Schindelin et al. 2012). We manually flipped and/or rotated leaves to standardize the direction of leaf shape asymmetries.

------------------------------------------------------------------------------------------------------------
Folder: Original_scans-Arb2015

Description: Original .tif scans of leaves from progeny plants grown in the common garden in 2015. File names correspond to the unique plant ID position number assigned to plants in the common garden (POS.Arb). 385 files.

Protocol details: We pressed, dried, weighed, and scanned one fully expanded undamaged leaf from the mid-section of each plant for morphometric analysis. We collected the leaves during the last week of August 2015 (from surviving plants with an intact leaf). We scanned each undamaged, fully-expanded, mid-canopy leaf at 300 dpi on a flatbed scanner.

------------------------------------------------------------------------------------------------------------
Folder: Original_scans-Field2014

Description: Original .tif scans of leaves from wild mother plants growing in remote home field sites in 2014. File names are FIELDSITE_MomID.2014 aka the field site ID and the unique ID assigned to each measured plant within the temporary plot in each field site. 1141 files. 

Protocol details: We pressed, dried, weighed, and scanned one fully expanded undamaged leaf from the mid-section of each plant for morphometric analysis. Leaves were collected between August 9 and September 23, 2014. See Date.Measured column in input_data/ field_2014_morphologicaldata-3-22-16.csv for collection date for each leaf. We scanned each undamaged, fully-expanded, mid-canopy leaf at 300 dpi on a flatbed scanner.

------------------------------------------------------------------------------------------------------------
Citation:
Schindelin, J., I Arganda-Carreras, E. Frise, V. Kaynig, M. Longair, T. Pietzsch, ... J. Y. Tinevez. 2012. Fiji: an open-source platform for biological-image analysis. Nat. Methods 9:676-682.

------------------------------------------------------------------------------------------------------------
- End of file -
