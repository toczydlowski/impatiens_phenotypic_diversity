#README

CANOPY OPENNESS DATA
------------------------------------------------------------------------------------------------------------
Folder: Canopy_photos_and_GLA_outputs

Description: Original photos of canopy taken in center of remote home field site plots, processed using the program Gap Light Analyzer (GLA), to measure the percent of open canopy above each field site. Original input photos and derived GLA images are included for the 8 of 12 sites where canopy photos were taken. The other 4 field sites and the common garden do not have canopy photos. 32 files.

------------------------------------------------------------------------------------------------------------
Files: Canopy_analysis_in_GLA.xxx

Description: Output of GLA analysis to calculate the percent of open canopy above each site. 3 files.

Columns names – description; units:

User.Field.1 – remote home field site name; unitless
X..Cnpy.Open – open canopy above plot; percentage

------------------------------------------------------------------------------------------------------------
- End of file -
