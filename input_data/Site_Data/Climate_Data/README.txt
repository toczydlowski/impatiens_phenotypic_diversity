#README

CLIMATE DATA
------------------------------------------------------------------------------------------------------------
File: Biotron_G127_Temperature_Data.csv

Description: Temperature data collected by the University of Wisconsin-Madison Biotron for the growing room (G127) where the Impatiens capensis seeds for the 2015 common garden were planted and germinated (before transplanting to the common garden). Not used in actual analyses.

Columns names – description; units:

Date – date and time that temperature was collected; month/day/year hr:min
Temperature-C – air temperature; degrees C

------------------------------------------------------------------------------------------------------------
Folder: 1985-2016data_perday_fieldsites

Description: Climate data from the PRISM Climate Group (2014, https://prism.oregonstate.edu/recent/) for the 4×4 km grid square containing each of the 12 remote home field sites. The .png files are screenshots of the PRISM download settings and location of grid cell data were retrieved from for each site. The .csv files are the climate data. Additional details from PRISM are in the header of the .csv files. 24 files.

File name key: 
FieldSiteID-PRISM_climateVariables_gridResolution_startDate_endDate_latitudeOfPRISMcell_longitudeOfPRISMcell.csv

Columns names for .csv's – description; units:
Date – date climate data were collected; month/day/year
ppt (mm) – daily amount of precipitation; mm
tmin (degrees C) –  daily minimum temperature; degrees C
tmean (degrees C) – daily mean temperature; degrees C
tmax (degrees C) – daily maximum temperature; degrees C
vpdmin (hPa) – daily minimum vapor pressure deficit; hPa
vpdmax (hPa) – daily maximum vapor pressure deficit; hPa

------------------------------------------------------------------------------------------------------------
Folder: 2014-2015data_perday_cg

Description: Climate data from the PRISM Climate Group (2014, https://prism.oregonstate.edu/recent/) for the 4×4 km grid square containing the common garden for 1/01/2014 – 12/31/2015, which includes the timeframe the progeny were in the common garden. Data include: daily precipitation; daily mean, minimum, and maximum temperature; and daily minimum and maximum vapor pressure deficit (VPD) and were downloaded per day (daily values option). The .png file is a screenshot of the PRISM download settings used to retrieve the data. The .csv file is the climate data. Additional details from PRISM are in the header of the .csv file. 2 files.

Columns names for .csv's – description; units:
Date – date climate data were collected; month/day/year
ppt (mm) – daily amount of precipitation; mm
tmin (degrees C) –  daily minimum temperature; degrees C
tmean (degrees C) – daily mean temperature; degrees C
tmax (degrees C) – daily maximum temperature; degrees C
vpdmin (hPa) – daily minimum vapor pressure deficit; hPa
vpdmax (hPa) – daily maximum vapor pressure deficit; hPa

------------------------------------------------------------------------------------------------------------
- End of file -
