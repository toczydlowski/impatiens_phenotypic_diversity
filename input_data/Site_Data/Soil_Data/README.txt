#README

SOIL DATA

------------------------------------------------------------------------------------------------------------
filename: soils.csv

Description: Soil nutrient and physical properties data for 12 remote home field sites and the common garden.

Columns names – description; units:

SampleID – name of the remote field site (UWArb is the common garden); unitless
RIVER – name of the rivers sytem field site is close to; unitless
WTSHED – name of the greater watershed the remote field site is within; unitless
pH – soil pH; unitless
OM – soil organic matter; percentage
TN – total nitrogen; percentage
P – soil phosphorous; ppm
K – soil potassium; ppm
Ca – soil calcium; ppm
Mg – soil magnesium; ppm
Sand – amount of sand; percentage
Silt – amount of silt; percentage
Clay – amount of clay; percentage
Name – textural name for soil type (based on sand/silt/clay percentages); unitless

------------------------------------------------------------------------------------------------------------
filename: Scan of submitted forms for soil analysis.pdf

Description: Scan of the forms that we submitted to the lab that processed the soil samples.

------------------------------------------------------------------------------------------------------------
Protocol details:
In late Fall 2014 and 2015, we collected and pooled five 3×10 cm soil cores from random locations within each home site and the common garden for analysis at the Wisconsin Soil & Plant Analysis Lab. These measurements included: pH, organic matter, total nitrogen, available phosphorous (with a sulfuric acid extracting solution), exchangeable potassium, calcium, and magnesium, and the percent sand, silt, and clay for each of the 13 pooled samples.

------------------------------------------------------------------------------------------------------------
Pertinent notes sent to soil lab:
All samples are from field sites in natural stands in Wisconsin southern lowland forests.

Multiple soil samples (4-6) were collected per site and pooled into one sample for analysis.  One Ziplock bag represents a site, 13 sites total.

I will use this data in regression models that try to explain morphological differences in an understory herb (jewelweed).  Therefore, I am interested in accurate data, but not necessarily recommendations for how to address deficiencies.  You have my permission to skip the recommendations altogether and just provide the raw data in the report.

All samples were dried for at least 48 hrs at 50-55C.  All samples were then ground and stored in sealed Ziplock bags.  These instructions came from previous phone conversations with your lab.

As these samples were from floodplain forests, the samples were incredibly wet when collected (many sites were flooded/underwater), and the soil itself usually had very high water holding capacity.  Therefore, the samples shrank in volume considerably when dried and ground and did not result in 2 cups per sample.  In previous phone conversations with your lab about this possible issue, I was told that 50g for the forest soils analysis and 50g (or as low as 25g) for the physical particle analysis per sample would be enough.  Each sample should be 100g or greater.  I do not anticipate there being much soil remaining for most samples, and therefore will likely not want this small amount of soil to be shipped back to Madison. 

Contact Info:
Rachel Toczydlowski
UW Madison Botany, Waller Lab - Graduate student 
Email: toczydlowski@wisc.edu

------------------------------------------------------------------------------------------------------------
Info received from soil lab with data:

SOIL and FORAGE ANALYSIS LABORATORY	
2611 Yellowstone Drive, Marshfield, WI  54449			
Phone 715-387-2523 ext 11		
			
University of Wisconsin Madison	
College of Agriculture and Life Sciences
Soil Science Department	

Rachel Toczydlowski	
UW-Madison, Botany Department		
430 Lincoln Dr		
Madison WI  53706			

Date	5/27/16	
Acct #	557837
Lab #	3305

Forest Soil Analysis			
Available Nutrients
Physical Analysis

------------------------------------------------------------------------------------------------------------
- End of file -
