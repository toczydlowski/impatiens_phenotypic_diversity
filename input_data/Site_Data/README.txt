#README

SITE DATA

------------------------------------------------------------------------------------------------------------

Description: This folder contains all environmental data used in analyses. Data are present for each of the 12 remote home field sites and the common garden site. Canopy data are from fisheye photos. Climate data are from PRISM climate group. Soil data are from pooled soil cores taken in each site and analyzed by UW Soils Lab. See README in each specific subfolder for details. 3 folders.

- End of file -
