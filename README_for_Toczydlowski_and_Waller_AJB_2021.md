# README #

This repository houses some data files and all code associated with Toczydlowski and Waller. 2021. Plastic and quantitative genetic divergence mirror environmental gradients among wild, fragmented populations of <i>Impatiens capensis</i>. American Journal of Botany.
A full package of all original data and scripts as they were at the time of publication is aviable on Dryad at: https://doi.org/10.5061/dryad.0cfxpnw36

### How do I get set up? ###

* Clone the repository to your local computer.
* If you use RStudio, double click the .Rproj file at the top level of the repo.
* Run the scripts you want to (they are numbered in the order they are meant to be run).
* If you don't use RStudio, open the scripts you would like to run as you do other R scripts and edit any file paths in them accordingly. 

### Who do I talk to? ###

Corresponding author:
Rachel Toczydlowski, rhtoczydlowski[at]gmail[dot]com, https://orcid.org/0000-0002-8141-2036


### Repository details ###
Paper: Plastic and quantitative genetic divergence mirror environmental gradients among wild, fragmented populations of <i>Impatiens capensis</i>

Publication Year: 2021

Journal: <i>American Journal of Botany</i>

Authors: Rachel H. Toczydlowski and Donald M. Waller

Subfolders contain more detailed READMEs when applicable.

Folders:

1. figures/: Contains all figures for the paper including supplemental figures. All of these figures can be produced using the code in /scripts. 22 files.

2. output_data_files/: Includes derived intermediate data files used in downstream analyses. All of these files can be produced using the code in /scripts. 21 files.

3. input_data/: Includes all original data files used in analyses. Data include digital images of scanned leaves, plant morphometric and reproductive data, site data including soil, canopy openness, and climate data for the remote field sites and common garden, and genetic diversity and pairwise Fst measures for the 12 populations analyzed. 4 files and 2 folders.

4. scripts/: Includes .R scripts that include all analyses for the paper. These scripts generate all files in /figures and /output_data_files. The scripts are numbered in the order that they should be run. 12 files.

Note that this package is housed on Dryad in full.


A subset of files are also a git repository on BitBucket at:
https://toczydlowski@bitbucket.org/toczydlowski/impatiens_phenotypic_diversity

- End of file -